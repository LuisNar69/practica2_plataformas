let URL = 'https://computacion.unl.edu.ec/pdml/practica1/';

//devolver la url
export function url_api() {
    return URL;
}

//documentos
export async function obtenerDoc(recurso, key = "") {
    let headers = {};

    headers = {
        'TOKEN-KEY': key
    };

    const response = await (await fetch(URL + recurso,{
        method: "GET",
        headers: headers,
        cache: 'no-store'
    })).json();

    return response;
}

//Buscar documento
export async function Documento(recurso, key = "") {
    let headers = {};

    headers = {
        'TOKEN-KEY': key
    };

    const response = await (await fetch(URL + recurso,{
        method: "GET",
        headers: headers,
        cache: 'no-store'
    })).json();

    return response;
}

//cambiar estado documentos
export async function cambiarEstado(recurso, key = "") {
    let headers = {};

    headers = {
        'TOKEN-KEY': key
    };

    const response = await (await fetch(URL + recurso,{
        method: "GET",
        headers: headers,
        cache: 'no-store'
    })).json();

    return response;
}

//Obtener Materias
export async function obtenerMaterias(recurso) {
    const datos = await (await fetch(URL + recurso, {
        method: "GET",
    })).json();
    return datos;
}

//enviar peticion
export async function enviar(recurso, data, key = "") {
    let headers = {};

    if (key !== "") {
        headers = {
            'TOKEN-KEY': key
        };
    } else {
        headers = {

        };
    }

    const response = await fetch(URL + recurso, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    });

    return await response.json();
}
'use client';
import mensajes from "@/componentes/Mensajes";
import {  cambiarEstado, obtenerDoc } from "@/hooks/Conexion";
import { borrarSesion, getId, getToken } from "@/hooks/SessionUtilClient";
import Link from "next/link";
import { useState } from "react";

export default function Page() {

    let key = getToken();
    const external = getId();
    const [documentos, setDocumentos] = useState([]);
    const [llamada, setLlamada] = useState(false);

    //llamar documentos
    if (!llamada) {
        obtenerDoc('documento.php?funcion=listar_documento_user&external=' + external, key).then((info) => {
            if (info.code === 200) {
                setDocumentos(info.datos);
                setLlamada(true);
            } else {
                mensajes("No se pudo Listar los documentos", "Error", "error");
            }
        });
    };

    //cambiar estado documentos
    const handleEliminar = (external) => {
        let key = getToken();
        cambiarEstado('documento.php?funcion=cambiar_estado&external=' + external + '&estado=false', key).then((info) => {
            if (info.code === 200) {
                mensajes("Estado cambiado con exito", "Eliminar", "success");
                setLlamada(false);
            } else {
                mensajes("No se pudo cambiar el estado", "Error", "error");
            }
        });
    };


    const salir = () => {
        borrarSesion();
     }

    return (

        <div className="row">
            <h1 style={{ textAlign: "center" }}> Documentos</h1>
            <div className="container-fluid">
                <div className="col-12 mb-4" style={{ alignItems: "center" }}>

                    <Link style={{ margin: "15px" }} href="/documentos/nuevo" className="btn btn-success font-weight-bold">Registrar</Link>
                    <Link style={{ margin: "15px" }} href="/inicio_sesion"  onClick={salir} className="btn btn-info font-weight-bold">Salir</Link>

                </div>
                <table className="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nro</th>
                            <th>Titulo</th>
                            <th>Autor</th>
                            <th>Paginas</th>
                            <th>Isbn</th>
                            <th>Materia</th>
                            <th>Estado</th>
                            <th>foto</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {documentos.map((dato, i) => (
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{dato.titulo}</td>
                                <td>{dato.autor}</td>
                                <td>{dato.paginas}</td>
                                <td>{dato.isbn}</td>
                                <td>{dato.nombre}</td>
                                <td>{dato.estado}</td>
                                <td><img src={dato.foto} alt="" style={{width: 50, height:50}}/></td>
                                <td>
                                    {<Link style={{ margin: "5px" }} href={`/documentos/editar/${dato.external}`} className="btn btn-warning font-weight-bold">Modificar</Link>}
                                    <button type="btn" onClick={() => handleEliminar(dato.external)}  style={{ margin: "5px" }} className="btn btn-danger font-weight-bold"> Eliminar</button>
                                </td>
                            </tr>
                        )
                        )}
                    </tbody>
                </table>

            </div>
        </div>
    );
}



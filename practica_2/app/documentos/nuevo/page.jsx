'use client';
import mensajes from "@/componentes/Mensajes";
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import Link from "next/link";
import { useRouter } from 'next/navigation';
import { enviar, obtenerMaterias } from "@/hooks/Conexion";
import { useState } from "react";
import { getId, getToken } from "@/hooks/SessionUtilClient";

export default function Page() {

  const router = useRouter();

  const external = getId();

  const [materias, setMaterias] = useState([]);
  const [llamada, setLlamada] = useState(false);

  //llamar materias
  if (!llamada) {
    obtenerMaterias('documento.php?funcion=materias').then((info) => {
      if (info.code === 200) {
        setMaterias(info.datos);
        setLlamada(true);
      } else {
        mensajes("No se pudo cargar las materias", "Error", "error");
      }
    });
  };

  //validaciones
  const validationShema = Yup.object().shape({
    titulo: Yup.string().required('Ingrese un titulo'),
    autor: Yup.string().required('ingrese un autor'),
    subtotal: Yup.number().required('Ingrese un Subtotal'),
    iva: Yup.number().required('ingrese iva'),
    total: Yup.number().required('Ingrese total'),
    descuento: Yup.number().required('ingrese el descuento'),
    isbn: Yup.string().required('Ingrese un isbn'),
    paginas: Yup.number().required('ingrese nro de paginas'),
    foto: Yup.string().required('Ingrese una imagen'),
    // user: Yup.string().required('ingrese su clave'),
    external_materia: Yup.string().required('Seleccione una materia'),
  });

  const formOptions = { resolver: yupResolver(validationShema) };
  const { register, handleSubmit, formState } = useForm(formOptions);
  const { errors } = formState;

  //Metodo para guardar documento
  const sendData = (data) => {
    var datos = {
      'titulo': data.titulo,
      'autor': data.autor,
      'subtotal': data.subtotal,
      'iva': data.iva,
      'total': data.total,
      'descuento': data.descuento,
      'isbn': data.isbn,
      'paginas': data.paginas,
      'foto': data.foto,
      'user': external,
      'materia': data.external_materia,
      "funcion": "guardarDocumento"
    };

    let key = getToken();
    enviar('documento.php', datos, key).then((info) => {
      if (info.code !== 200) {
        mensajes("Documento no se pudo modificar", "Error", "error")
      } else {
        mensajes("Documento guardado correctamente", "Informacion", "success")
        router.push("/documentos");
      }
    });
  };


  return (
    <div className="wrapper" >
      <center>
        <br /><br />
        <div className="d-flex flex-column" style={{ width: 700 }}>
          <h5 className="title" style={{ color: "black", font: "bold" }}>REGISTRO DOCUMENTOS</h5>
          <br />

          <div className='container-fluid'>
            <form className="user" onSubmit={handleSubmit(sendData)}>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('titulo')} name="titulo" id="titulo" placeholder="Ingrese el titulo" className={`form-control ${errors.titulo ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.titulo?.message}</div>
                </div>
                <div className="col">
                  <input {...register('autor')} name="autor" id="autor" placeholder="Ingrese el autor" className={`form-control ${errors.autor ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.autor?.message}</div>
                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('subtotal')} name="subtotal" id="subtotal" placeholder="Ingrese el subtotal" className={`form-control ${errors.subtotal ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.subtotal?.message}</div>
                </div>
                <div className="col">
                  <input {...register('iva')} name="iva" id="iva" placeholder="Ingrese el iva" className={`form-control ${errors.iva ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.iva?.message}</div>
                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('total')} name="total" id="total" placeholder="Ingrese el total" className={`form-control ${errors.total ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.total?.message}</div>
                </div>
                <div className="col">
                  <input {...register('descuento')} name="descuento" id="descuento" placeholder="Ingrese el descuento" className={`form-control ${errors.descuento ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.descuento?.message}</div>
                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('isbn')} name="isbn" id="isbn" placeholder="Ingrese un isbn" className={`form-control ${errors.isbn ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.isbn?.message}</div>
                </div>
                <div className="col">
                  <input {...register('paginas')} name="paginas" id="paginas" placeholder="Ingrese nro paginas" className={`form-control ${errors.paginas ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.paginas?.message}</div>
                </div>
              </div>

              <div className="row mb-4">
                <div className="col">
                  <input {...register('foto')} name="foto" id="foto" placeholder="Ingrese url foto" className={`form-control ${errors.foto ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.foto?.message}</div>
                </div>
                <div className="col">
                  <select {...register('external_materia')} name="external_materia" id="external_materia" className={`form-control ${errors.external_materia ? 'is-invalid' : ''}`}>
                    <option>Elija una materia</option>
                    {materias.map((aux, i) => {
                      return (<option key={i} value={aux.external_id}>
                        {aux.nombre}
                      </option>)
                    })}
                  </select>
                  <div className='alert alert-danger invalid-feedback'>{errors.external_materia?.message}</div>
                </div>
              </div>


              <hr />
              <button type='submit' className="btn btn-success">GUARDAR</button>

            </form>
          </div>
          {<Link style={{ margin: "5px" }} href="/documentos" className="btn btn-danger font-weight-bold">Regresar</Link>}
        </div>
      </center>
    </div>
  );
}


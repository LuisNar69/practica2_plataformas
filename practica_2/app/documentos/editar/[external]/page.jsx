'use client';
import mensajes from "@/componentes/Mensajes";
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import Link from "next/link";
import { useRouter } from 'next/navigation';
import { Documento, enviar, obtenerMaterias } from "@/hooks/Conexion";
import { useEffect, useState } from "react";
import { getToken } from "@/hooks/SessionUtilClient";

export default function Page({ params }) {
  const { external } = params;
  let key = getToken();

  const router = useRouter();

 
  const [materias, setMaterias] = useState([]);
  const [documento, setDocumento] = useState([]);
  const [llamada, setLlamada] = useState(false);
  const [llamada2, setLlamada2] = useState(false);

  //llamar materias
  if (!llamada) {
    obtenerMaterias('documento.php?funcion=materias').then((info) => {
      if (info.code === 200) {
        setMaterias(info.datos);
        setLlamada(true);
      } else {
        mensajes("No se pudo cargar las materias", "Error", "error");
      }
    });
  };

  //buscar documento
  if (!llamada2) {
    Documento("documento.php?funcion=obtener_documento&external=" + external, key).then((info) => {
      if (info.code === 200) {
        const documentoData = info.datos[0];
        setDocumento(documentoData);
        setLlamada2(true);
      } else {
        mensajes("No se pudo Listar los documentos", "Error", "error");
      }
    });
  };

  //validaciones
  const validationShema = Yup.object().shape({
    titulo: Yup.string().required('Ingrese un titulo'),
    autor: Yup.string().required('ingrese un autor'),
    subtotal: Yup.number().required('Ingrese un Subtotal'),
    iva: Yup.number().required('ingrese iva'),
    total: Yup.number().required('Ingrese total'),
    descuento: Yup.number().required('ingrese el descuento'),
    isbn: Yup.string().required('Ingrese un isbn'),
    paginas: Yup.number().required('ingrese nro de paginas'),
    foto: Yup.string().required('Ingrese una imagen'),
    // user: Yup.string().required('ingrese su clave'),
    external_materia: Yup.string().required('Seleccione una materia'),
  });

  const formOptions = { resolver: yupResolver(validationShema) };
  const { register, handleSubmit, setValue, formState } = useForm(formOptions);
  const { errors } = formState;

  //Metodo para guardar documento
  const sendData = (data) => {
    var datos = {
      'titulo': data.titulo,
      'autor': data.autor,
      'subtotal': data.subtotal,
      'iva': data.iva,
      'total': data.total,
      'descuento': data.descuento,
      'isbn': data.isbn,
      'paginas': data.paginas,
      'foto': data.foto,
      'external': external,
      'materia': data.external_materia,
      "funcion": "modificarDocumento"
    };

    let key = getToken();
    enviar('documento.php', datos, key).then((info) => {
      if (info.code !== 200) {
        mensajes("Documento no se pudo modificar", "Error", "error")
      } else {
        mensajes("Documento modificado correctamente", "Informacion", "success")
        router.push("/documentos");
      }
    });
  };

  useEffect(() => {
    if (documento) {
      setValue('titulo', documento.titulo);
      setValue('autor', documento.autor);
      setValue('subtotal', documento.subtotal);
      setValue('iva', documento.iva);
      setValue('total', documento.total);
      setValue('descuento', documento.descuento);
      setValue('isbn', documento.isbn);
      setValue('paginas', documento.paginas);
      setValue('foto', documento.foto);
      setValue('isbn', documento.isbn);
    }
  }, [documento, setValue]);


  return (
    <div className="wrapper" >
      <center>
        <br /><br />
        <div className="d-flex flex-column" style={{ width: 700 }}>
          <h5 className="title" style={{ color: "black", font: "bold" }}>MODIFICAR DOCUMENTOS</h5>
          <br />

          <div className='container-fluid'>
            <form className="user" onSubmit={handleSubmit(sendData)}>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('titulo')} name="titulo" id="titulo"    className={`form-control ${errors.titulo ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.titulo?.message}</div>
                </div>
                <div className="col">
                  <input {...register('autor')} name="autor" id="autor" className={`form-control ${errors.autor ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.autor?.message}</div>
                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('subtotal')} name="subtotal" id="subtotal"  className={`form-control ${errors.subtotal ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.subtotal?.message}</div>
                </div>
                <div className="col">
                  <input {...register('iva')} name="iva" id="iva" className={`form-control ${errors.iva ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.iva?.message}</div>
                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('total')} name="total" id="total"  className={`form-control ${errors.total ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.total?.message}</div>
                </div>
                <div className="col">
                  <input {...register('descuento')} name="descuento" id="descuento"  className={`form-control ${errors.descuento ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.descuento?.message}</div>
                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('isbn')} name="isbn" id="isbn" className={`form-control ${errors.isbn ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.isbn?.message}</div>
                </div>
                <div className="col">
                  <input {...register('paginas')} name="paginas" id="paginas" className={`form-control ${errors.paginas ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.paginas?.message}</div>
                </div>
              </div>

              <div className="row mb-4">
                <div className="col">
                  <input {...register('foto')} name="foto" id="foto" className={`form-control ${errors.foto ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.foto?.message}</div>
                </div>
                <div className="col">
                  <select {...register('external_materia')} name="external_materia" id="external_materia" className={`form-control ${errors.external_materia ? 'is-invalid' : ''}`}>
                    <option>Elija una materia</option>
                    {materias.map((aux, i) => {
                      return (<option key={i} value={aux.external_id}>
                        {aux.nombre}
                      </option>)
                    })}
                  </select>
                  <div className='alert alert-danger invalid-feedback'>{errors.external_materia?.message}</div>
                </div>
              </div>


              <hr />
              <button type='submit' className="btn btn-success">GUARDAR</button>

            </form>
          </div>
          {<Link style={{ margin: "5px" }} href="/documentos" className="btn btn-danger font-weight-bold">Regresar</Link>}
        </div>
      </center>
    </div>
  );
}

